package com.gregdev.whirldroid;

import android.annotation.TargetApi;
import android.app.backup.BackupAgentHelper;
import android.app.backup.SharedPreferencesBackupHelper;

@TargetApi(8)
public class PreferenceBackupAgent extends BackupAgentHelper {
	
	// key to uniquely identify the set of backup data
	private static final String PREFS_BACKUP_KEY = "whirldroidprefs";
	
	@Override
	public void onCreate() {
		SharedPreferencesBackupHelper helper = new SharedPreferencesBackupHelper(this, this.getPackageName () + "_preferences");
		addHelper(PREFS_BACKUP_KEY, helper);
	}
	
}
