package com.gregdev.whirldroid;

public class WhirlpoolApiException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public WhirlpoolApiException(String message) {
		super(message);
	}
	
}
