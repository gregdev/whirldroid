package com.gregdev.whirldroid.activity;

import java.util.regex.Pattern;

import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.text.util.Linkify;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.gregdev.whirldroid.R;
import com.gregdev.whirldroid.Whirldroid;

public class About extends SherlockActivity {

	private TextView version_info;
	private TextView gregdev_url;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		setTheme(Whirldroid.getWhirldroidTheme());
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about);
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		version_info = (TextView) findViewById(R.id.version_info);
		gregdev_url = (TextView) findViewById(R.id.gregdev_web);
		
		Pattern p_url = Pattern.compile("http://gregdev.com.au");
		Linkify.addLinks(gregdev_url, p_url, "http://");

		String version = null;
		try {
			version = this.getPackageManager().getPackageInfo("com.gregdev.whirldroid", 0).versionName;
			version_info.setText(String.format(getString(R.string.version_info), version));
		}
		catch (NameNotFoundException e) { }

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				finish();
				return true;
		}
		 
		return super.onOptionsItemSelected(item);
	}

}