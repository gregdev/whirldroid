package com.gregdev.whirldroid.activity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.google.android.gms.analytics.Tracker;
import com.gregdev.whirldroid.R;
import com.gregdev.whirldroid.Whirldroid;
import com.gregdev.whirldroid.WhirlpoolApi;

public class Dashboard extends SherlockActivity {

	private Button news_button;
	private Button whims_button;
	private Button watched_button;
	private Button recent_button;
	private Button popular_button;
	private Button forums_button;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		setTheme(Whirldroid.getWhirldroidTheme());
		super.onCreate(savedInstanceState);
		
		switch (Whirldroid.getCurrentThemeId()) {
			case Whirldroid.DARK_THEME:
				setContentView(R.layout.dashboard_dark);
				break;
			case Whirldroid.LIGHT_THEME:
			default:
				setContentView(R.layout.dashboard);
				break;
		}
		
		
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		String api_key = settings.getString("pref_apikey", null);
		
		if (api_key == null) {
			Intent intent = new Intent(this, Login.class);
			finish();
			startActivity(intent);
		}

		findAllViewsById();

		news_button.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent news_intent = new Intent(Dashboard.this, NewsList.class);
				Dashboard.this.startActivity(news_intent);
			}
		});

		whims_button.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent whim_intent = new Intent(Dashboard.this, WhimList.class);
				Dashboard.this.startActivity(whim_intent);
			}
		});

		watched_button.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent watched_intent = new Intent(Dashboard.this, ThreadList.class);
				
				Bundle bundle = new Bundle();
				bundle.putInt("forum_id", WhirlpoolApi.WATCHED_THREADS);
				watched_intent.putExtras(bundle);
				
				Dashboard.this.startActivity(watched_intent);
			}
		});

		recent_button.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent recent_intent = new Intent(Dashboard.this, ThreadList.class);
				
				Bundle bundle = new Bundle();
				bundle.putInt("forum_id", WhirlpoolApi.RECENT_THREADS);
				recent_intent.putExtras(bundle);
				
				Dashboard.this.startActivity(recent_intent);
			}
		});
		
		popular_button.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent popular_intent = new Intent(Dashboard.this, ThreadList.class);
				
				Bundle bundle = new Bundle();
				bundle.putInt("forum_id", WhirlpoolApi.POPULAR_THREADS);
				popular_intent.putExtras(bundle);
				
				Dashboard.this.startActivity(popular_intent);
			}
		});
		
		forums_button.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent forums_intent = new Intent(Dashboard.this, ForumList.class);
				Dashboard.this.startActivity(forums_intent);
			}
		});
		
	    Tracker t = ((Whirldroid) getApplication()).getTracker(Whirldroid.TrackerName.APP_TRACKER);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.main_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.menu_about:
				Intent about_intent = new Intent(this, About.class);
				startActivity(about_intent);
				return true;
			case R.id.menu_settings:
				Intent settings_intent = new Intent(this, Settings.class);
				startActivity(settings_intent);
				return true;
			case R.id.menu_feedback:
				// create email intent
				final Intent email_intent = new Intent(android.content.Intent.ACTION_SEND);

				// add email data to the intent
				email_intent.setType("plain/text");
				email_intent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"greg@gregdev.com.au"});
				email_intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Whirldroid feedback");
				email_intent.putExtra(android.content.Intent.EXTRA_TEXT, "");

				startActivity(Intent.createChooser(email_intent, "Send mail..."));
				return true;
		}
		return false;
	}

	private void findAllViewsById() {
		whims_button   = (Button) findViewById(R.id.btn_whims);
		news_button    = (Button) findViewById(R.id.btn_news);
		watched_button = (Button) findViewById(R.id.btn_watched);
		recent_button  = (Button) findViewById(R.id.btn_recent);
		popular_button = (Button) findViewById(R.id.btn_popular);
		forums_button  = (Button) findViewById(R.id.btn_forums);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		// theme preference has been changed
		if (Whirldroid.hasThemeChanged()) {
			Whirldroid.log("theme changed, refreshing view");
			Whirldroid.setThemeChanged(false);
			
			// restart the activity
			Intent i = getBaseContext().getPackageManager().getLaunchIntentForPackage(getBaseContext().getPackageName());
	        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	        startActivity(i);
		}
		
	    SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
	    
	    long interval = Long.parseLong(settings.getString("pref_notifyfreq", "0"));
	    interval = interval * 60 * 1000;
	    
	    boolean notify_whim    = settings.getBoolean("pref_whimnotify", false);
		boolean notify_watched = settings.getBoolean("pref_watchednotify", false);
	    
	    AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
	    Intent i = new Intent(this, com.gregdev.whirldroid.service.NotificationService.class);
	    PendingIntent pi = PendingIntent.getService(this, 0, i, 0);
	    am.cancel(pi);
	    
	    if (interval > 0 && (notify_whim || notify_watched)) {
	        am.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
	        		SystemClock.elapsedRealtime(), interval, pi);
	    }
	}

}