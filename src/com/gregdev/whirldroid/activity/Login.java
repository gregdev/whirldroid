package com.gregdev.whirldroid.activity;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.util.Linkify;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.gregdev.whirldroid.R;
import com.gregdev.whirldroid.Whirldroid;
import com.gregdev.whirldroid.WhirlpoolApiException;

public class Login extends SherlockActivity {

    private ProgressDialog progress_dialog;
    private RetrieveDataTask task;

    private class RetrieveDataTask extends AsyncTask<String, Void, Boolean> {

        private String error_message = "";

        @Override
        protected Boolean doInBackground(String... params) {
            Login.this.runOnUiThread(new Runnable() {
                public void run() {
                    try {
                        progress_dialog = ProgressDialog.show(Login.this, "Just a sec...", "Verifying your API key...", true, true);
                        progress_dialog.setOnCancelListener(new CancelTaskOnCancelListener(task));
                    }
                    catch (WindowManager.BadTokenException e) { }
                }
            });
            try {
                List<String> get = new ArrayList<String>();
                get.add("forum");
                get.add("whims");
                get.add("news");
                get.add("recent");
                get.add("watched");

                Whirldroid.getApi().downloadData(get, null);
            }
            catch (final WhirlpoolApiException e) {
                error_message = e.getMessage();
                return false;
            }

            return true;
        }

        @Override
        protected void onPostExecute(final Boolean result) {
            runOnUiThread(new Runnable() {
                public void run() {
                if (progress_dialog != null) {
                    try {
                        progress_dialog.dismiss(); // hide the progress dialog
                        progress_dialog = null;
                    }
                    catch (Exception e) { }
                }

                // got data, API key must be valid
                if (result) {
                    // go to main dashboard
                    Intent intent = new Intent(Login.this, Dashboard.class);
                    finish();
                    startActivity(intent);
                }

                // no data, API key is probably invalid (or error on Whirlpool side)
                else {
                    // unset the API key setting
                    SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                    SharedPreferences.Editor settingsEditor = settings.edit();
                    settingsEditor.putString("pref_apikey", null);
                    settingsEditor.commit();

                    AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);
                    builder.setMessage("It looks like there might be a problem with your API key. Please check your key and remember to include dashes.")
                        .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            }
                        });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
                }
            });
        }
    }

    /**
     * Cancels the fetching of data if the back button is pressed
     */
    private class CancelTaskOnCancelListener implements DialogInterface.OnCancelListener {
        private AsyncTask<?, ?, ?> task;
        public CancelTaskOnCancelListener(AsyncTask<?, ?, ?> task) {
            this.task = task;
        }

        public void onCancel(DialogInterface dialog) {
            if (task != null) {
                task.cancel(true);
            }
        }
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        setTheme(Whirldroid.getWhirldroidTheme());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        
        Button login_button  = (Button) findViewById(R.id.login_btn);
        final EditText api_key_edit = (EditText) findViewById(R.id.api_key_field);
        
        login_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                // store the API key
                String api_key = api_key_edit.getText().toString();

                SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                SharedPreferences.Editor settingsEditor = settings.edit();
                settingsEditor.putString("pref_apikey", api_key);
                settingsEditor.commit();

                task = new RetrieveDataTask(); // start new thread to retrieve data
                task.execute();
            }
        });
        
        TextView apiKeyWhere = (TextView) findViewById(R.id.api_key_where);
        
        Pattern p_url = Pattern.compile("http://whirlpool.net.au/profile/");
        Linkify.addLinks(apiKeyWhere, p_url, "http://");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getSupportMenuInflater();
        inflater.inflate(R.menu.about_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_about:
                Intent aboutIntent = new Intent(this, About.class);
                startActivity(aboutIntent);
                return true;
        }
        return false;
    }
}
