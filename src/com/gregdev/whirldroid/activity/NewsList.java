package com.gregdev.whirldroid.activity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager.BadTokenException;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockListActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.gregdev.whirldroid.WhirlpoolApi;
import com.gregdev.whirldroid.R;
import com.gregdev.whirldroid.Whirldroid;
import com.gregdev.whirldroid.WhirlpoolApiException;
import com.gregdev.whirldroid.layout.SeparatedListAdapter;
import com.gregdev.whirldroid.model.NewsArticle;

/**
 * Displays the latest Whirlpool news in a nice list format
 * @author Greg
 *
 */
public class NewsList extends SherlockListActivity {

	private SeparatedListAdapter sla;
	private ArrayList<NewsArticle> news_list;
	private ProgressDialog progress_dialog;
	private RetrieveNewsTask task;

	/**
	 * Private class to retrieve news in the background
	 * @author Greg
	 *
	 */
	private class RetrieveNewsTask extends AsyncTask<String, Void, ArrayList<NewsArticle>> {

		private boolean clear_cache = false;
		private String error_message = "";
		
		public RetrieveNewsTask(boolean clear_cache) {
			this.clear_cache = clear_cache;
		}

		@Override
		protected ArrayList<NewsArticle> doInBackground(String... params) {
			if (clear_cache || Whirldroid.getApi().needToDownloadNews()) {
				NewsList.this.runOnUiThread(new Runnable() {
				    public void run() {
				    	try {
					    	progress_dialog = ProgressDialog.show(NewsList.this, "Just a sec...", "Loading news...", true, true);
							progress_dialog.setOnCancelListener(new CancelTaskOnCancelListener(task));
				    	}
				    	catch (BadTokenException e) { }
				    }
				});
				try {
					Whirldroid.getApi().downloadNews();
				}
				catch (final WhirlpoolApiException e) {
					error_message = e.getMessage();
					return null;
				}
			}
			news_list = Whirldroid.getApi().getNewsArticles();
			return news_list;
		}

		@Override
		protected void onPostExecute(final ArrayList<NewsArticle> result) {
			runOnUiThread(new Runnable() {
				public void run() {
					if (progress_dialog != null) {
						try {
							progress_dialog.dismiss(); // hide the progress dialog
							progress_dialog = null;
						}
						catch (Exception e) { }
						
						if (result != null) {
							Toast.makeText(NewsList.this, "News refreshed", Toast.LENGTH_SHORT).show();
						}
					}
					if (result != null) {
						setNews(news_list); // display the news in the list
					}
					else {
						Toast.makeText(NewsList.this, error_message, Toast.LENGTH_LONG).show();
					}
				}
			});
		}
	}

	/**
	 * A private class to format the news list items
	 * @author Greg
	 *
	 */
	public class NewsAdapter extends ArrayAdapter<NewsArticle> {

		private ArrayList<NewsArticle> news_articles;

		public NewsAdapter(Context context, int textViewResourceId, ArrayList<NewsArticle> newsItems) {
			super(context, textViewResourceId, newsItems);
			this.news_articles = newsItems;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;
			if (v == null) {
				LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.list_row, null);
			}
			NewsArticle article = news_articles.get(position);
			if (article != null) {
				TextView tt = (TextView) v.findViewById(R.id.top_text);
				TextView bt = (TextView) v.findViewById(R.id.bottom_text);
				if (tt != null) {
					tt.setText(article.getTitle());
				}
				if (bt != null){
					bt.setText(article.getBlurb());
				}
			}
			return v;
		}

	}

	/**
	 * Cancels the fetching of news if the back button is pressed
	 * @author Greg
	 *
	 */
	private class CancelTaskOnCancelListener implements OnCancelListener {
		private AsyncTask<?, ?, ?> task;
		public CancelTaskOnCancelListener(AsyncTask<?, ?, ?> task) {
			this.task = task;
		}

		public void onCancel(DialogInterface dialog) {
			if (task != null) {
				task.cancel(true);
			}
		}
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		setTheme(Whirldroid.getWhirldroidTheme());
		super.onCreate(savedInstanceState);
		setContentView(R.layout.news_list);
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		getNews(false);
	}
	
	@Override
	public void onRestart() {
		super.onRestart();
		getNews(false);
	}

	/**
	 * Opens news item in Web browser when list item is pressed
	 */
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		NewsArticle article = (NewsArticle) sla.getItem(position);
		String news_url = "http://whirlpool.net.au/news/go.cfm?article=" + article.getId();

		Intent news_intent = new Intent(Intent.ACTION_VIEW, Uri.parse(news_url));
		startActivity(news_intent);
	}


	private void getNews(boolean clear_cache) {
		task = new RetrieveNewsTask(clear_cache); // start new thread to retrieve the news
		task.execute();
	}

	/**
	 * Loads the news items into the list
	 * @param news_list News items
	 */
	private void setNews(ArrayList<NewsArticle> news_list) {
		long last_updated = System.currentTimeMillis() / 1000 - Whirldroid.getApi().getNewsLastUpdated();
		
		if (last_updated < 10) { // updated less than 10 seconds ago
			getSupportActionBar().setSubtitle("Updated just a moment ago");
		}
		else {
			String ago = Whirldroid.getTimeSince(last_updated);
			getSupportActionBar().setSubtitle("Updated " + ago + " ago");
		}
		
		if (news_list == null || news_list.size() == 0) { // no news found
			return;
		}

		sla = new SeparatedListAdapter(this);

		int current_day = -1;
		String current_day_name = null;
		ArrayList<NewsArticle> articles = new ArrayList<NewsArticle>();

		for (NewsArticle article : news_list) {
			Date article_date = article.getDate();
			Calendar cal = Calendar.getInstance();
			cal.setTime(article_date);
			int day = cal.get(Calendar.DAY_OF_WEEK);
			
			if (day != current_day) { // new day
				if (!articles.isEmpty()) {
					NewsAdapter na = new NewsAdapter(this, android.R.layout.simple_list_item_1, articles);
					sla.addSection(current_day_name, na);
					//articles.clear();
					articles = new ArrayList<NewsArticle>();
				}
				current_day = day;
				current_day_name = getDayName(day);
			}
			articles.add(article);
		}
		if (!articles.isEmpty()) {
			NewsAdapter na = new NewsAdapter(this, android.R.layout.simple_list_item_1, articles);
			sla.addSection(current_day_name, na);
		}
		setListAdapter(sla); // display the news
	}

	private String getDayName(int day) {
		switch (day) {
			case Calendar.SUNDAY:
				return "Sunday";
			case Calendar.MONDAY:
				return "Monday";
			case Calendar.TUESDAY:
				return "Tuesday";
			case Calendar.WEDNESDAY:
				return "Wednesday";
			case Calendar.THURSDAY:
				return "Thursday";
			case Calendar.FRIDAY:
				return "Friday";
			case Calendar.SATURDAY:
				return "Saturday";
			default:
				return null;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.refresh, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.menu_refresh:
				long now = System.currentTimeMillis() / 1000;
				// don't refresh too often
				if (now - Whirldroid.getApi().getNewsLastUpdated() > WhirlpoolApi.REFRESH_INTERVAL) {
					getNews(true);
				}
				else {
					Toast.makeText(NewsList.this, "Wait " + WhirlpoolApi.REFRESH_INTERVAL + " seconds before refreshing", Toast.LENGTH_SHORT).show();
				}
				return true;

		 	case android.R.id.home:
		 		Intent dashboard_intent = new Intent(this, Dashboard.class);
				dashboard_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(dashboard_intent);
		 		return true;
		}
		return false;
	}
}