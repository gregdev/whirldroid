package com.gregdev.whirldroid.activity;

import android.annotation.TargetApi;
import android.app.backup.BackupManager;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceManager;

import com.actionbarsherlock.app.SherlockPreferenceActivity;
import com.actionbarsherlock.view.MenuItem;
import com.gregdev.whirldroid.R;
import com.gregdev.whirldroid.Whirldroid;

public class Settings extends SherlockPreferenceActivity {
	
	OnSharedPreferenceChangeListener listener;
	
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(Whirldroid.getWhirldroidTheme());
		super.onCreate(savedInstanceState);
		
		addPreferencesFromResource(R.layout.preferences);
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
		
		listener = new SharedPreferences.OnSharedPreferenceChangeListener() {
			@TargetApi(8)
			public void onSharedPreferenceChanged(SharedPreferences settings, String key) {
				// theme preference may have changed, update it
				Whirldroid.setCurrentTheme(true);
				
				try {
		            Class.forName("android.app.backup.BackupManager");
		            BackupManager.dataChanged(getPackageName());
		        }
				catch (ClassNotFoundException e) {
		        	// backups not available on this device, meh
		        }
			}
		};
		
		settings.registerOnSharedPreferenceChangeListener(listener);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				finish();
				return true;
		}
		 
		return super.onOptionsItemSelected(item);
	}
}
