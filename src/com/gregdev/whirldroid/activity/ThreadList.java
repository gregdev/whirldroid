package com.gregdev.whirldroid.activity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.method.DigitsKeyListener;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager.BadTokenException;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.app.SherlockListActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.SearchView;
import com.actionbarsherlock.widget.SearchView.OnQueryTextListener;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.gregdev.whirldroid.R;
import com.gregdev.whirldroid.Whirldroid;
import com.gregdev.whirldroid.WhirlpoolApi;
import com.gregdev.whirldroid.WhirlpoolApiException;
import com.gregdev.whirldroid.layout.SeparatedListAdapter;
import com.gregdev.whirldroid.model.Forum;
import com.gregdev.whirldroid.model.Thread;

/**
 * Displays threads in a nice list format
 * @author Greg
 *
 */
public class ThreadList extends SherlockListActivity implements OnNavigationListener, OnQueryTextListener {

	private SeparatedListAdapter threads_adapter;
	private ThreadAdapter threads_adapter_no_headings;
	private Forum forum;
	private List<Thread> thread_list;
	private ProgressDialog progress_dialog;
	private RetrieveThreadsTask task;
	private WatchThreadTask watch_task;
	Map<String, List<Thread>> sorted_threads;
	private String forum_title;
	private int forum_id;
	private int list_position;
	private int current_page = 1;
	private int current_group = 0;
	private ListView thread_listview;
	private TextView no_threads;
	private GroupAdapter group_adapter;
	private Map<String, Integer> groups;
	
	private int search_forum = -1;
	private int search_group = -1;
	private String search_query;
	
	private class WatchThreadTask extends AsyncTask<String, Void, Boolean> {
		private int thread_id;
		
		public WatchThreadTask(int thread_id) {
			this.thread_id = thread_id;
		}
		
		@Override
		protected Boolean doInBackground(String... params) {
			try {
				Whirldroid.getApi().downloadWatched(0, 0, thread_id);
				return true;
			}
			catch (final WhirlpoolApiException e) {
				return false;
			}
		}
		
		@Override
		protected void onPostExecute(final Boolean result) {
			runOnUiThread(new Runnable() {
				public void run() {
					if (progress_dialog != null) {
						try {
							progress_dialog.dismiss(); // hide the progress dialog
							progress_dialog = null;
						}
						catch (Exception e) { }
					}
					if (result) {
						Toast.makeText(ThreadList.this, "Added thread to watch list", Toast.LENGTH_SHORT).show();
					}
					else {
						Toast.makeText(ThreadList.this, "Error downloading data", Toast.LENGTH_LONG).show();
					}
				}
			});
		}
	}
	
	private class MarkReadTask extends AsyncTask<String, Void, Boolean> {
		private int thread_id;
		
		public MarkReadTask(int thread_id) {
			this.thread_id = thread_id;
		}
		
		@Override
		protected Boolean doInBackground(String... params) {
			try {
				Whirldroid.getApi().downloadWatched(thread_id, 0, 0);
				return true;
			}
			catch (final WhirlpoolApiException e) {
				return false;
			}
		}
		
		@Override
		protected void onPostExecute(final Boolean result) {
			// do nothing
		}
	}

	/**
	 * Private class to retrieve threads in the background
	 * @author Greg
	 *
	 */
	private class RetrieveThreadsTask extends AsyncTask<String, Void, List<Thread>> {

		private boolean clear_cache = false;
		private int mark_thread_as_read = 0;
		private int unwatch_thread = 0;
		//private String error_message = "";
		
		public RetrieveThreadsTask(boolean clear_cache, int mark_thread_as_read, int unwatch_thread) {
			this.clear_cache = clear_cache;
			this.mark_thread_as_read = mark_thread_as_read;
			this.unwatch_thread = unwatch_thread;
		}
		
		public RetrieveThreadsTask(boolean clear_cache) {
			this.clear_cache = clear_cache;
		}

		@Override
		protected List<Thread> doInBackground(String... params) {			
			if (clear_cache || Whirldroid.getApi().needToDownloadThreads(forum_id)) {
				ThreadList.this.runOnUiThread(new Runnable() {
				    public void run() {
				    	try {
				    		String message;
				    		if (forum_id == WhirlpoolApi.SEARCH_RESULTS) {
				    			message = "Searching threads...";
				    		}
				    		else {
				    			message = "Loading threads...";
				    		}
					    	progress_dialog = ProgressDialog.show(ThreadList.this, "Just a sec...", message, true, true);
							progress_dialog.setOnCancelListener(new CancelTaskOnCancelListener(task));
				    	}
				    	catch (BadTokenException e) { }
				    }
				});
				try {
					switch (forum_id) {
						case WhirlpoolApi.WATCHED_THREADS:
							Whirldroid.getApi().downloadWatched(mark_thread_as_read, unwatch_thread, 0);
							break;
						case WhirlpoolApi.RECENT_THREADS:
							Whirldroid.getApi().downloadRecent();
							break;
						case WhirlpoolApi.POPULAR_THREADS:
							Whirldroid.getApi().downloadPopularThreads();
							break;
						case WhirlpoolApi.SEARCH_RESULTS:
							forum = new Forum(forum_id, "Search Results", 0, null);
							thread_list = Whirldroid.getApi().searchThreads(search_forum, search_group, search_query);
							return thread_list;
					}
				}
				catch (final WhirlpoolApiException e) {
					//error_message = e.getMessage();
					return null;
				}
			}
			
			forum = Whirldroid.getApi().getThreads(forum_id, current_page, current_group);
			
			if (forum == null) { // error downloading data
				return null;
			}
			
			thread_list = forum.getThreads();
			
			return thread_list;
		}

		@Override
		protected void onPostExecute(final List<Thread> result) {
			runOnUiThread(new Runnable() {
				public void run() {
					if (progress_dialog != null) {
						try {
							progress_dialog.dismiss(); // hide the progress dialog
							progress_dialog = null;
						}
						catch (Exception e) { }
						
						if (result != null && !isActualForum() && forum_id != WhirlpoolApi.SEARCH_RESULTS) {
							Toast.makeText(ThreadList.this, "Threads refreshed", Toast.LENGTH_SHORT).show();
						}
					}
					
					if (result != null) {
						if (groups == null) {
							groups = forum.getGroups();
						}
						if (groups != null) {
							group_adapter.clear();
							group_adapter.add(forum_title);
							
							String current_group_name = "";
							for (Map.Entry<String, Integer> group : groups.entrySet()) {
								group_adapter.add(group.getKey());
								if (group.getValue() == current_group) {
									current_group_name = group.getKey();
								}
							}
							if (current_group != 0) {
								getSupportActionBar().setSelectedNavigationItem(group_adapter.getPosition(current_group_name));
							}
						}
						
						invalidateOptionsMenu();
						SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
						
						if (thread_list != null && thread_list.size() == 0) {
							boolean hide_read = settings.getBoolean("pref_hidewatchedunread", true);
							if (forum_id == WhirlpoolApi.WATCHED_THREADS && hide_read) {
								no_threads.setText(getResources().getText(R.string.no_threads_unread));
							}
							else {
								no_threads.setText(getResources().getText(R.string.no_threads));
							}
							no_threads.setVisibility(View.VISIBLE);
						}
						else {
							no_threads.setVisibility(View.GONE);
						}
						
						if (!isActualForum()) {
							setThreads(thread_list); // display the threads in the list
						}
						else {
							//ThreadList.this.getSupportActionBar().setSubtitle("Page " + current_page + " of " + forum.getPageCount());
							setThreadsNoHeadings(thread_list);
						}
					}
					else {
						Toast.makeText(ThreadList.this, "Error downloading threads, please try again", Toast.LENGTH_LONG).show();
					}
				}
			});
		}
	}

	/**
	 * A private class to format the thread list items
	 * @author Greg
	 *
	 */
	public class ThreadAdapter extends ArrayAdapter<Thread> {

		private List<Thread> thread_items;
		boolean ignore_own;
		boolean hide_read;

		public ThreadAdapter(Context context, int textViewResourceId, List<Thread> thread_items) {
			super(context, textViewResourceId, thread_items);
			this.thread_items = thread_items;
			
			SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
			ignore_own = settings.getBoolean("pref_ignoreownreplies", false);
			hide_read = settings.getBoolean("pref_hidewatchedunread", true);
		}
		
		/**
		 * The next two methods are here to avoid issues caused by the system recycling views.
		 * This method returns an integer which identifies the view we should use for the
		 * corresponding list item
		 */
		public int getItemViewType(int position) {
		    Thread item = thread_items.get(position);
		    if (item.isSticky()) {
		    	return 1; // highlight as sticky
		    }
		    
		    if (item.isClosed()) {
		    	return 4; // highlight as closed
		    }
		    
		    if (item.isMoved()) {
		    	return 5; // highlight as moved
		    }
		    	
		    if (item.isDeleted()) {
		    	return 3; // highlight (fade out) as deleted
		    }
		    
		    else if (!hide_read && item.hasUnreadPosts()) {
		    	if (item.getLastPosterId().equals(Whirldroid.getOwnWhirlpoolId()) && ignore_own) {
		    		return 0; // last reply was by us, no highlighting
		    	}
		    	return 2; // highlight as unread
		    }
		    
		    return 0; // normal, no highlighting
		}

		/**
		 * This method needs to return the number of different item view layouts we have
		 * eg. sticky + unread + normal = 3, so return 3
		 */
		public int getViewTypeCount() {
		    return 6;
		}

		@Override
		public View getView(int position, View convert_view, ViewGroup parent) {
			Thread thread = thread_items.get(position);
			int type = getItemViewType(position);
			
			if (convert_view == null) {
				LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convert_view = vi.inflate(R.layout.list_row_thread, null);
				switch (type) {
					case 1:
						convert_view = vi.inflate(R.layout.list_row_sticky, null);
						break;
					case 2:
						convert_view = vi.inflate(R.layout.list_row_highlighted, null);
						break;
					case 3:
						convert_view = vi.inflate(R.layout.list_row_deleted, null);
						break;
					case 4:
						convert_view = vi.inflate(R.layout.list_row_closed, null);
						break;
					case 5:
						convert_view = vi.inflate(R.layout.list_row_moved, null);
						break;
					default:
						convert_view = vi.inflate(R.layout.list_row_thread, null);
						break;
				}
			}
			if (thread != null) {
				TextView title_textview = (TextView) convert_view.findViewById(R.id.top_text);
				TextView pagecount_textview = (TextView) convert_view.findViewById(R.id.bottom_right_text);
				TextView lastpost_textview = (TextView) convert_view.findViewById(R.id.bottom_left_text);
				
				if (title_textview != null) {
					if (thread.getUnread() > 0) {
						title_textview.setText(thread.getTitle() + " (" + thread.getUnread() + " unread)");
					}
					else {
						title_textview.setText(thread.getTitle());
					}
				}
				
				if (!thread.isDeleted() && pagecount_textview != null) {
					pagecount_textview.setText("" + thread.getPageCount());
				}
				
				if (lastpost_textview != null) {
					Date date = thread.getLastDate();
					if (thread.isDeleted()) {
						lastpost_textview.setText("This thread has been deleted");
					}
					else if (thread.isMoved()) {
						lastpost_textview.setText("This thread has been moved");
					}
					else if (date != null) {
						String time_text = Whirldroid.getTimeSince(date);
						lastpost_textview.setText(time_text + " ago by " + thread.getLastPoster());
					}
					else {
						lastpost_textview.setText(thread.getLastDateText() + " by " + thread.getLastPoster());
					}
				}
			}
			return convert_view;
		}
	}
	
	public class GroupAdapter extends ArrayAdapter<String> {
		
		List<String> group_items;
		Context context;
		
		public GroupAdapter(Context context, int resource, List<String> group_items) {
			super(context, resource, group_items);
			this.group_items = group_items;
			this.context = context;
		}
		
		@Override
		public View getView(int position, View convert_view, ViewGroup parent) {
			String group_name = group_items.get(position);
			
			if (convert_view == null) {
				LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convert_view = vi.inflate(R.layout.spinner_item, null);
			}
			if (group_name != null) {
				TextView title = (TextView) convert_view.findViewById(R.id.title);
				TextView subtitle = (TextView) convert_view.findViewById(R.id.subtitle);
				
				if (title != null) {
					title.setText(group_name);
				}
				if (subtitle != null) {
					String subtitle_value = "Page " + current_page;
					if (forum != null) {
						subtitle_value += " of " + forum.getPageCount();
					}
					subtitle.setText(subtitle_value);
				}
			}
			return convert_view;
		}
	}

	/**
	 * Cancels the fetching of threads if the back button is pressed
	 * @author Greg
	 *
	 */
	private class CancelTaskOnCancelListener implements OnCancelListener {
		private AsyncTask<?, ?, ?> task;
		public CancelTaskOnCancelListener(AsyncTask<?, ?, ?> task) {
			this.task = task;
		}

		public void onCancel(DialogInterface dialog) {
			if (task != null) {
				task.cancel(true);
			}
		}
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		setTheme(Whirldroid.getWhirldroidTheme());
		super.onCreate(savedInstanceState);
		setContentView(R.layout.thread_list);
		
		no_threads = (TextView) findViewById(R.id.no_threads);
		
		// save reference to listview
		thread_listview = (ListView) findViewById(android.R.id.list);
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		Bundle bundle = this.getIntent().getExtras();
		if (bundle != null) {
		    forum_id = bundle.getInt("forum_id");
		    
		    switch(forum_id) {
			    case WhirlpoolApi.WATCHED_THREADS:
			    	setTitle("Watched Threads");
			    	break;
			    case WhirlpoolApi.RECENT_THREADS:
			    	setTitle("Recent Threads");
			    	break;
			    case WhirlpoolApi.POPULAR_THREADS:
			    	setTitle("Popular Threads");
			    	break;
			    case WhirlpoolApi.SEARCH_RESULTS:
			    	setTitle("Search Results");
			    	search_query = bundle.getString("search_query");
			    	search_forum = bundle.getInt("search_forum");
			    	search_group = bundle.getInt("search_group");
			    	getSupportActionBar().setSubtitle("\"" + search_query + "\"");
			    	break;
			    default:
			    	forum_title = bundle.getString("forum_name");
			    	setTitle(forum_title);
			    	break;
		    }
		}

		getThreads(false);
		
		registerForContextMenu(getListView());
		
		if (this.isActualForum() && WhirlpoolApi.isPublicForum(forum_id)) {
			getSupportActionBar().setDisplayShowTitleEnabled(false);
			
			Context context = getSupportActionBar().getThemedContext();
			
			ArrayList<String> group_list = new ArrayList<String>();
			group_list.add(bundle.getString("forum_name"));
			
			group_adapter = new GroupAdapter(context, R.layout.spinner_item, group_list);
			
			group_adapter.setDropDownViewResource(R.layout.sherlock_spinner_dropdown_item);
	
			getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
			getSupportActionBar().setListNavigationCallbacks(group_adapter, this);
		}
		
		Tracker t = ((Whirldroid) getApplication()).getTracker(Whirldroid.TrackerName.APP_TRACKER);
	    t.setScreenName("Thread List: " + getTitle());
	    t.send(new HitBuilders.ScreenViewBuilder().build());
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		// go to last list position
		thread_listview.setSelection(list_position);
	}
	
	@Override
	public void onRestart() {
		super.onRestart();
		if (forum_id == WhirlpoolApi.WATCHED_THREADS) {
			// refresh watched thread list so "mark read" changes come through
			getThreads(false);
		}
	}
	
	@Override
	public void onPause() {
		super.onPause();
		// save list position
		list_position = thread_listview.getFirstVisiblePosition();
	}

	/**
	 * Opens thread in Web browser when list item is pressed
	 */
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		
		Thread thread = null;
		if (threads_adapter == null) {
			thread = (Thread) threads_adapter_no_headings.getItem(position);
		}
		else {
			thread = (Thread) threads_adapter.getItem(position);
		}
		
		if (thread.isDeleted()) { // check if the thread is deleted
			// can't open a deleted thread!
			return;
		}
		
		openThread(thread, 1, false);
	}
	
	/**
	 * Display a context menu when a thread is long pressed
	 */
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menu_info) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) menu_info;
		int pos = info.position;
		
		Thread t = null;
		if (!isActualForum()) {
			t = Whirldroid.getThreadAtPosition(pos, sorted_threads);
		}
		else {
			t = thread_list.get(pos);
		}
		
		menu.setHeaderTitle(R.string.ctxmenu_thread);
		if (forum_id == WhirlpoolApi.WATCHED_THREADS) {
			if (t.getUnread() > 0) {
				menu.add(Menu.NONE, 0, 0, getResources().getText(R.string.ctxmenu_mark_as_read));
			}
			menu.add(Menu.NONE, 1, 1, getResources().getText(R.string.ctxmenu_unwatch));
		}
		else {
			menu.add(Menu.NONE, 2, 0, getResources().getText(R.string.ctxmenu_watch_thread));
		}
		
		menu.add(Menu.NONE, 3, 2, getResources().getText(R.string.ctxmenu_go_to_last_post));
		
		if (!isActualForum()) {
			menu.add(Menu.NONE, 4, 3, getResources().getText(R.string.ctxmenu_go_to_forum));
		}
	}
	
	/**
	 * Context menu item selection
	 */
	@Override
	public boolean onContextItemSelected(android.view.MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		int pos = info.position;
		
		Thread t = null;
		if (!isActualForum()) {
			t = Whirldroid.getThreadAtPosition(pos, sorted_threads);
		}
		else {
			t = thread_list.get(pos);
		}
		
		switch (item.getItemId()) {
			case 0: // mark as read
				getThreads(true, t.getId(), 0);
				return true;
			case 1: // unwatch
				getThreads(true, 0, t.getId());
				return true;
			case 2: // watch
				markThreadAsWatched(t.getId());
				return true;
			case 3: // open at last post
				openThread(t, -1, true);
				return true;
			case 4: // go to forum
				openForum(t.getForumId(), t.getForum());
				return true;
			case 5: // open in browser
				openThreadInBrowser(t, 1, false, 0);
				return true;
				
		}
		
		return false;
	}
	
	public void markThreadAsWatched(int thread_id) {
		progress_dialog = ProgressDialog.show(this, "Just a sec...", "Watching thread...", true, true);
		progress_dialog.setOnCancelListener(new CancelTaskOnCancelListener(task));
		watch_task = new WatchThreadTask(thread_id); // start new thread to retrieve threads
		watch_task.execute();
	}

	private void getThreads(boolean clear_cache) {
		task = new RetrieveThreadsTask(clear_cache); // start new thread to retrieve threads
		task.execute();
	}

	private void getThreads(boolean clear_cache, int mark_thread_as_read, int unwatch_thread) {
		task = new RetrieveThreadsTask(clear_cache, mark_thread_as_read, unwatch_thread); // start new thread to retrieve threads
		task.execute();
	}
	
	private void setThreadsNoHeadings(List<Thread> thread_list) {
		if (thread_list == null || thread_list.size() == 0) { // no threads found
			return;
		}

		threads_adapter_no_headings = new ThreadAdapter(this, R.layout.list_row, thread_list);
		setListAdapter(threads_adapter_no_headings);
	}

	/**
	 * Loads the thread item into the list
	 * @param thread_list Threads
	 */
	private void setThreads(List<Thread> thread_list) {
		long last_updated = System.currentTimeMillis() / 1000;
		switch (forum_id) {
			case WhirlpoolApi.RECENT_THREADS:
				last_updated -= Whirldroid.getApi().getRecentLastUpdated();
				break;
			case WhirlpoolApi.WATCHED_THREADS:
				last_updated -= Whirldroid.getApi().getWatchedLastUpdated();
				break;
			case WhirlpoolApi.POPULAR_THREADS:
				last_updated -= Whirldroid.getApi().getPopularLastUpdated();
				break;
		}
		
		if (WhirlpoolApi.isPublicForum(forum_id) || forum_id == WhirlpoolApi.POPULAR_THREADS
				|| forum_id == WhirlpoolApi.RECENT_THREADS || forum_id == WhirlpoolApi.WATCHED_THREADS) {
			if (last_updated < 10) { // updated less than 10 seconds ago
				getSupportActionBar().setSubtitle("Updated just a moment ago");
			}
			else {
				String ago = Whirldroid.getTimeSince(last_updated);
				getSupportActionBar().setSubtitle("Updated " + ago + " ago");
			}
		}
		
		if (thread_list == null || thread_list.size() == 0) { // no threads found
			setListAdapter(null);
			return;
		}

		threads_adapter = new SeparatedListAdapter(this);
		
		sorted_threads = Whirldroid.groupThreadsByForum(thread_list);
		
		for (Map.Entry<String, List<Thread>> entry : sorted_threads.entrySet()) {
			String forum_name = entry.getKey();
			List<Thread> threads = entry.getValue();
			ThreadAdapter ta = new ThreadAdapter(this, android.R.layout.simple_list_item_1, threads);
			threads_adapter.addSection(forum_name, ta);
		}

		setListAdapter(threads_adapter);
	}
	
	private void openThread(Thread thread, int page_number, boolean bottom) {
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

		boolean load_in_browser        = settings.getBoolean("pref_openthreadsinbrowser", false);
		boolean load_recent_at_top     = settings.getBoolean("pref_loadattop", false);
		boolean load_watched_at_top    = settings.getBoolean("pref_loadwatchedattop", false);
		boolean load_public_at_bottom  = settings.getBoolean("pref_loadpublicatbottom", false);
		boolean load_private_at_bottom = settings.getBoolean("pref_loadprivateatbottom", false);
		boolean auto_mark_read         = settings.getBoolean("pref_watchedautomarkasread", false);
		
		int goto_post = 0;

		// this is a list of watched threads, and the preference is to open the thread at the last read post
		if (!bottom && !load_watched_at_top && forum_id == WhirlpoolApi.WATCHED_THREADS) {
			page_number = thread.getLastPage();
			goto_post = thread.getLastPost();
			bottom = false;
		}

		// this is a list of recent threads, and the preference is to open the thread at the bottom
		if (!bottom && !load_recent_at_top && forum_id == WhirlpoolApi.RECENT_THREADS) {
			page_number = -1;
			bottom = true;
		}

        // this is a public thread, and the preference is to open the thread at the bottom
        if (load_public_at_bottom && isActualForum() && WhirlpoolApi.isPublicForum(forum_id)) {
            page_number = -1;
            bottom = true;
        }

        // this is a private thread, and the preference is to open the thread at the bottom
        if (load_private_at_bottom && isActualForum() && !WhirlpoolApi.isPublicForum(forum_id)) {
            page_number = -1;
            bottom = true;
        }
		
		if (auto_mark_read && thread.hasUnreadPosts()) {
			MarkReadTask mark_read = new MarkReadTask(thread.getId()); // start new thread to retrieve threads
			mark_read.execute();
		}
		
		// set to open threads in browser
		if (load_in_browser) {
			openThreadInBrowser(thread, page_number, bottom, goto_post);
		}
		
		// forum is private and cannot be scraped
		else if (!WhirlpoolApi.isPublicForum(thread.getForumId())) {
			openThreadInBrowser(thread, page_number, bottom, goto_post);
		}

		// open the thread within Whirldroid
		else {
			openThreadInApp(thread, page_number, bottom, goto_post);
		}
	}
	
	private void openThreadInApp(Thread thread, int page_number, boolean bottom, int goto_post) {
		Intent thread_intent = new Intent(getApplicationContext(), ThreadView.class);
		
		if (goto_post != 0) {
			goto_post = (goto_post % WhirlpoolApi.POSTS_PER_PAGE) - 1;
			if (goto_post == -1) {
				goto_post = WhirlpoolApi.POSTS_PER_PAGE;
			}
		}
		
		Bundle bundle = new Bundle();
		bundle.putInt("thread_id", thread.getId());
		bundle.putString("thread_title", thread.getTitle());
		bundle.putInt("page_number", page_number);
		bundle.putBoolean("bottom", bottom);
		bundle.putInt("goto_num", goto_post);
		bundle.putInt("from_forum", forum_id);
		thread_intent.putExtras(bundle);
		
		startActivity(thread_intent);
	}
	
	private void openThreadInBrowser(Thread thread, int page_number, boolean bottom, int goto_post) {
		String thread_url = "http://forums.whirlpool.net.au/forum-replies.cfm?t=" + thread.getId();
		
		if (bottom) {
			thread_url += "&p=-1#bottom";
		}
		else if (goto_post != 0) {
			thread_url += "&p=" + page_number + "#r" + goto_post;
		}

		Intent thread_intent = new Intent(Intent.ACTION_VIEW, Uri.parse(thread_url));
		startActivity(thread_intent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
		if (isActualForum()) {
			inflater.inflate(R.menu.forum, menu);
			if (android.os.Build.VERSION.SDK_INT >= 8) {
		        //Create the search view
		        SearchView search_view = new SearchView(getSupportActionBar().getThemedContext());
		        search_view.setQueryHint("Search for threads…");
		        search_view.setOnQueryTextListener(this);
		
		        menu.add("Search")
		            .setIcon(R.drawable.abs__ic_search)
		            .setActionView(search_view)
		            .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
			}
		}
		else {
			inflater.inflate(R.menu.refresh, menu);
		}
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// hide page navigation for private forums
		if (!WhirlpoolApi.isPublicForum(forum_id)) {
			try {
				menu.findItem(R.id.menu_prev).setVisible(false);
				menu.findItem(R.id.menu_next).setVisible(false);
				menu.findItem(R.id.menu_goto_page).setVisible(false);
				menu.findItem(R.id.menu_open_browser).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
				menu.findItem(R.id.menu_new_thread).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
			}
			catch (NullPointerException e) {
				// menu item may not exist, meh
			}
		}
		if (current_page == 1) {
			try {
				menu.findItem(R.id.menu_prev).setEnabled(false);
			}
			catch (NullPointerException e) { }
		}
		
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.menu_refresh:
				long now = System.currentTimeMillis() / 1000;
				// don't refresh too often
				if (now - Whirldroid.getApi().getRecentLastUpdated() > WhirlpoolApi.REFRESH_INTERVAL) {
					getThreads(true);
				}
				else {
					Toast.makeText(ThreadList.this, "Wait " + WhirlpoolApi.REFRESH_INTERVAL + " seconds before refreshing", Toast.LENGTH_LONG).show();
				}
				return true;
				
			case R.id.menu_prev:
				current_page--;
				getThreads(false, 0, 0);
				return true;
				
			case R.id.menu_goto_page:
				final EditText input = new EditText(this);
				input.setKeyListener(new DigitsKeyListener());
				new AlertDialog.Builder(this)
				    .setTitle("Jump to page...")
				    .setMessage("Enter a page number to load")
				    .setView(input)
				    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				        public void onClick(DialogInterface dialog, int whichButton) {
				            Editable value = input.getText();
				            try {
				            	current_page = Integer.parseInt(value.toString());
				            }
				            catch (Exception e) { }
							getThreads(false, 0, 0);
				        }
				    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				        public void onClick(DialogInterface dialog, int whichButton) {
				            // Do nothing.
				        }
				    }).show();
				return true;
				
			case R.id.menu_next:
				current_page++;
				getThreads(false, 0, 0);
				return true;
				
			case R.id.menu_new_thread:
				Intent newthread_intent = new Intent(Intent.ACTION_VIEW, Uri.parse(WhirlpoolApi.NEWTHREAD_URL + forum_id));
				startActivity(newthread_intent);
				return true;
				
			case R.id.menu_open_browser:
				Intent thread_intent = new Intent(Intent.ACTION_VIEW, Uri.parse(WhirlpoolApi.FORUM_URL + forum_id));
				startActivity(thread_intent);
				return true;

			case android.R.id.home:
				Intent dashboard_intent = new Intent(this, Dashboard.class);
				dashboard_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(dashboard_intent);
				return true;
		}
		return false;
	}
	
	/**
	 * Checks if the current threads are from an actual forum on Whirlpool,
	 * or are Whirldroid-specific (recent, watched, popular, etc)
	 * @return
	 */
	private boolean isActualForum() {
		if (
			forum_id == WhirlpoolApi.RECENT_THREADS ||
			forum_id == WhirlpoolApi.WATCHED_THREADS ||
			forum_id == WhirlpoolApi.POPULAR_THREADS ||
			forum_id == WhirlpoolApi.SEARCH_RESULTS
		) {
			return false;
		}
		
		return true;
	}
	
	private void openForum(int forum_id, String forum_name) {
		Intent forum_intent = new Intent(getApplicationContext(), ThreadList.class);
		Bundle bundle = new Bundle();
		bundle.putInt("forum_id", forum_id);
		bundle.putString("forum_name", forum_name);
		forum_intent.putExtras(bundle);
		startActivity(forum_intent);
	}

	public boolean onNavigationItemSelected(int item_position, long item_id) {
		try {
			if (item_position == 0 && current_group == 0) {
				return false;
			}
			else if (item_position == 0) {
				current_group = 0;
				current_page = 1;
				getThreads(true);
				return true;
			}

			int counter = 1;
			for (Map.Entry<String, Integer> group : groups.entrySet()) {
				if (counter == item_position) {
					if (current_group == group.getValue()) {
						return false;
					}
					
					current_group = group.getValue();
					current_page = 1;
					getThreads(true);
					return true;
				}
				counter++;
			}
			return false;
		}
		catch (NullPointerException e) {
			return false;
		}
	}

	public boolean onQueryTextSubmit(String query) {
		Intent search_intent;
		
		// private forums can't be searched, so open the browser
		if (!WhirlpoolApi.isPublicForum(forum_id)) {
			String search_url = WhirlpoolApi.buildSearchUrl(forum_id, -1, query);
			search_intent = new Intent(Intent.ACTION_VIEW, Uri.parse(search_url));
		}
		else {
			search_intent = new Intent(this, ThreadList.class);
			
			Bundle bundle = new Bundle();
			bundle.putInt("forum_id", WhirlpoolApi.SEARCH_RESULTS);
			bundle.putString("search_query", query);
			bundle.putInt("search_forum", forum_id);
			bundle.putInt("search_group", -1);
			
			search_intent.putExtras(bundle);
		}
		
		startActivity(search_intent);
		
		return true;
	}

	public boolean onQueryTextChange(String newText) {
		return false;
	}
}