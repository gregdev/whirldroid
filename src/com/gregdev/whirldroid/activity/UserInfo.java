package com.gregdev.whirldroid.activity;

import java.util.Map;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.WindowManager.BadTokenException;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.gregdev.whirldroid.R;
import com.gregdev.whirldroid.Whirldroid;
import com.gregdev.whirldroid.model.User;

public class UserInfo extends SherlockActivity {
	
	private TextView user_details;
	private User user;
	private ProgressDialog progress_dialog;
	private GetUserInfoTask task;
	
	private class GetUserInfoTask extends AsyncTask<String, Void, Void> {

		@Override
		protected Void doInBackground(String... params) {
			UserInfo.this.runOnUiThread(new Runnable() {
			    public void run() {
			    	try {
				    	progress_dialog = ProgressDialog.show(UserInfo.this, "Just a sec...", "Loading user info...", true, true);
						progress_dialog.setOnCancelListener(new CancelTaskOnCancelListener(task));
			    	}
			    	catch (BadTokenException e) { }
			    }
			});
			
			user.downloadInfo();
			
			return null;
		}

		@Override
		protected void onPostExecute(final Void result) {
			runOnUiThread(new Runnable() {
				public void run() {
					if (progress_dialog != null) {
						try {
							progress_dialog.dismiss(); // hide the progress dialog
							progress_dialog = null;
						}
						catch (Exception e) { }
					}
					
					setUserInfo();
				}
			});
		}
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		setTheme(Whirldroid.getWhirldroidTheme());
		super.onCreate(savedInstanceState);
		setContentView(R.layout.user_info);
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		user_details = (TextView) findViewById(R.id.user_details);
		
		Bundle bundle = this.getIntent().getExtras();
		if (bundle != null) {
		    user = bundle.getParcelable("user");
		}
		
		setTitle(user.getName());
		getSupportActionBar().setSubtitle("#" + user.getId());

		task = new GetUserInfoTask(); // start new thread to retrieve user info
		task.execute();
	}
	
	private void setUserInfo() {
		String info = "";
		
		for (Map.Entry<String, String> entry : user.getInfo().entrySet()) {
			info += entry.getKey() + " " + entry.getValue() + "\n\n";
		}
		
		user_details.setText(info);
	}
	
	/**
	 * Cancels the fetching of user info if the back button is pressed
	 */
	private class CancelTaskOnCancelListener implements OnCancelListener {
		private AsyncTask<?, ?, ?> task;
		public CancelTaskOnCancelListener(AsyncTask<?, ?, ?> task) {
			this.task = task;
		}

		public void onCancel(DialogInterface dialog) {
			if (task != null) {
				task.cancel(true);
			}
		}
	}
	
}
