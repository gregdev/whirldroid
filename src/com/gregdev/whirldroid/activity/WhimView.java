package com.gregdev.whirldroid.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.gregdev.whirldroid.R;
import com.gregdev.whirldroid.Whirldroid;
import com.gregdev.whirldroid.WhirlpoolApiException;
import com.gregdev.whirldroid.model.Whim;

public class WhimView extends SherlockActivity {
	private TextView whimContent;
	private Whim whim = null;
	
	private class MarkWhimAsReadTask extends AsyncTask<Whim, Integer, Integer> {

		@Override
		protected Integer doInBackground(Whim... whims) {
			try {
				Whirldroid.getApi().downloadWhims(whims[0].getId());
			}
			catch (WhirlpoolApiException e) {
				// error marking whim as read, meh
			}
			
			return null;
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		setTheme(Whirldroid.getWhirldroidTheme());
		super.onCreate(savedInstanceState);
		setContentView(R.layout.whim_view);
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		Bundle bundle = this.getIntent().getExtras();
		if (bundle != null) {
		    whim = bundle.getParcelable("whim");
		}

		whimContent = (TextView) findViewById(R.id.whim_content);
		whimContent.setText(whim.getContent());
		
		getSupportActionBar().setSubtitle("From " + whim.getFromName());
		
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		
		// if then whim is unread and the option to auto mark as read is selected
		if (!whim.isRead() && settings.getBoolean("pref_whimautomarkasread", true)) {
			new MarkWhimAsReadTask().execute(whim);
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.whim, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// hide the Mark as Read option if auto mark as read is enabled
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		if (settings.getBoolean("pref_whimautomarkasread", true)) {
			menu.findItem(R.id.menu_whimmarkread).setVisible(false);
		}
		
	    return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.menu_whimmarkread:
				new MarkWhimAsReadTask().execute(whim);
				Toast.makeText(this, "Marking whim as read", Toast.LENGTH_SHORT).show();
				return true;
				
			case R.id.menu_whimopenbrowser:
				String whim_url = "http://whirlpool.net.au/whim/?action=read&m=" + whim.getId();
				Intent whim_intent = new Intent(Intent.ACTION_VIEW, Uri.parse(whim_url));
				startActivity(whim_intent);
				return true;
				
			case R.id.menu_whimreply:
				String reply_url = "http://whirlpool.net.au/whim/?action=write&rt=" + whim.getId();
				Intent reply_intent = new Intent(Intent.ACTION_VIEW, Uri.parse(reply_url));
				startActivity(reply_intent);
				return true;
		
			case android.R.id.home:
				Intent dashboard_intent = new Intent(this, Dashboard.class);
				dashboard_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(dashboard_intent);
				return true;
		}
		 
		return super.onOptionsItemSelected(item);
	}
}