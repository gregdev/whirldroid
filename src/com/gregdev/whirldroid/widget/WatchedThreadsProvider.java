package com.gregdev.whirldroid.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.gregdev.whirldroid.R;

public class WatchedThreadsProvider extends AppWidgetProvider {

	@Override
	public void onUpdate(Context context, AppWidgetManager awm, int[] widget_ids) {
		final int N = widget_ids.length;
		
		// do stuff for each widget that belongs to this provider
		for (int i = 0; i < N; i++) {
			int widget_id = widget_ids[i];
			
			RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
					R.layout.widget_watched_threads);

			// Register an onClickListener
			Intent intent = new Intent(context, WatchedThreadsProvider.class);

			intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
			intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, widget_ids);

			PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
					0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
			remoteViews.setOnClickPendingIntent(R.id.textView1, pendingIntent);
			awm.updateAppWidget(widget_id, remoteViews);
		}
	}
	
	static void updateAppWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId, String titlePrefix) {
		RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget_watched_threads);
		// Tell the widget manager
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }
	
}
